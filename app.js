const express = require('express');
const path = require('path');
const logger = require('morgan');
const mongodb = require('mongodb')
const Grid = require('gridfs-stream');
const crypto = require('crypto');

const https = require('https');
const fs = require('fs');
const utils = require('./utils');

const apiRouter = require('./routes');
const dbUrl = 'mongodb://localhost:27017';

// app
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// connect to db and init
mongodb.MongoClient.connect(dbUrl, { useUnifiedTopology: true })
    .then(client => {
        // setup DB
        let obj = require('mongodb').ObjectId;
        ObjectId = raw => {
            try {
                return obj(raw);
            } catch (err) {
                return 'none';
            }
        };
        db = client.db('slapi');
        gfs = Grid(db, mongodb);
        // create a dummy user if there's none
        utils.dbUsers().countDocuments({ is_admin: true }).then(count => {
            if (count === 0) {
                const bcrypt = require('bcrypt');
                let password = crypto.randomBytes(10).toString('hex');
                bcrypt.hash(password, 10, (err, hash) => {
                    utils.dbUsers().insertOne({
                        name: 'root',
                        password: hash,
                        is_admin: true
                    });
                    console.log(`First admin user created: root / ${password}`)
                })
            }
        });
        // setup routes
        app.use('/api', apiRouter);
        // setup server
        https.createServer({
            key: fs.readFileSync('./private/key.pem'),
            cert : fs.readFileSync('./private/cert.pem')
        }, app).listen(8080, () => {
            console.log("Server started on port 8080");
            // setup printer history
            setInterval(() => {
                utils.dbPrinters().find({}).toArray().then(printers => {
                    for (let printer of printers) {
                        utils.connectToPrinterOrCached(null, printer, rpc => {
                            utils.printerInvokeAndResultOrEmptyValue(null, rpc, 'state', [], [], state => {
                                // printing has finished
                                if (state.printing && state.current_job.finished) {
                                    // add to history
                                    utils.getPrinterExtraProperty(null, rpc, 'loaded_file_id', loadedFileId => {
                                        utils.dbPrinterAddJobHistory(rpc, printer, 'success', loadedFileId, state.current_job.start_time * 1000);
                                    });
                                    // finish print
                                    utils.printerInvokeAndResultOrEmptyValue(null, rpc, 'finish_print', []);
                                }
                            });
                        });
                    }
                });
            }, 2500);
        });
    });

// export module
module.exports = app;
