const express = require('express');
const utils = require('../utils');

let router = express.Router();

let printerInfo = printer => {
    return {
        id: printer._id,
        name: printer.name,
        socket_address: printer.socket_address,
        properties: printer.properties
    };
};

let socketRegex = /^(ipc:\/\/[^\0]+)|(tcp|udp):\/\/(.*)(:[0-9]{1,5})?$/;

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//          PRINTER MANAGEMENT
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// get a list of printer information
router.get('/', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbPrinters().find().toArray().then(printers => {
            let result = [];
            for (let printer of printers) {
                result.push(printerInfo(printer));
            }
            utils.resValue(res, { printers: result });
        });
    });
});

// get printer information by id (without job history)
router.get('/:printerid', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.resValue(res, printerInfo(printer));
        });
    });
});

// post a printer (must be admin)
router.post('/', (req, res) => {
    utils.reqRequireParamsOrError(req, res, ['name', 'socket_address', 'properties'], () => {
        if (!socketRegex.test(req.body.socket_address)) {
            utils.resError(res, 'invalid data', 422, { param: 'socket_address', found: req.body.socket_address });
            return;
        }
        utils.validateAuthAdminOrError(req, res, user => {
            utils.dbFindPrinter({'name': req.body.name},
                () => {
                    // insert in db
                    utils.dbPrinters().insertOne({
                        name: req.body.name,
                        socket_address: req.body.socket_address,
                        properties: req.body.properties
                    }).then(insertResult => {
                        utils.resValue(res, { id: insertResult.insertedId });
                    });
                },
                // printer already exists
                printer => utils.resError(res, 'name already exists', 409)
            );
        });
    });
});

// modify printer (must be self or admin)
router.patch('/:printerid', (req, res) => {
    utils.validateAuthAdminOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            // validate arguments
            let setQuery = {};
            if (req.body.name) {
                setQuery['name'] = req.body.name;
            }
            if (req.body.socket_address) {
                if (!socketRegex.test(req.body.socket_address)) {
                    utils.resError(res, 'invalid data', 422);
                    return;
                }
                setQuery['socket_address'] = req.body.socket_address;
            }
            if (req.body.properties) {
                setQuery['properties'] = req.body.properties;
            }
            if (Object.keys(setQuery).length === 0) {
                utils.resError(res, 'invalid request', 422);
            } else {
                // modify user
                utils.dbPrinters().updateOne({'_id': printer._id}, { $set: setQuery }).then(updateResult => {
                    utils.resValue(res);
                    // close connection if socket address was updated
                    if (setQuery.socket_address) {
                        try {
                            utils.disconnectPrinter(printer);
                        } catch (err) {}
                    }
                });
            }
        });
    });
});

// delete a printer (must be admin)
router.delete('/:printerid', (req, res) => {
    utils.validateAuthAdminOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            // close connection
            try {
                utils.disconnectPrinter(printer);
            } catch (err) {}
            // delete
            utils.dbPrinters().deleteOne({ '_id': ObjectId(req.params.printerid) }).then(deleteResult => {
                if (deleteResult.deletedCount === 0) {
                    utils.resError(res);
                } else {
                    utils.resValue(res);
                }
            });
        });
    });
});

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//          PRINTER CONTROL
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// power a printer on
router.post('/:printerid/power', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                // attempt to power on
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'poweron', [], ['Already powered on']);
            });
        });
    });
});

// power a printer off
router.delete('/:printerid/power', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                // attempt to power off
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'poweroff', [], ['Already powered off']);
            });
        });
    });
});

// load file to printer
router.post('/:printerid/load', (req, res) => {
    utils.reqRequireParamsOrError(req, res, ['fileid'], () => {
        utils.validateAuthOrError(req, res, user => {
            utils.dbFindOwnedFileOrError(res, ObjectId(req.body.fileid), user._id, file => {
                utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
                    utils.connectToPrinterOrCached(res, printer, rpc => {
                        // attempt to read file
                        let buffers = [];
                        let hasError = false;
                        gfs.createReadStream({ filename: file.gfsname })
                            .on('data', buffer => buffers.push(buffer))
                            .on('error', err => {
                                console.log(err);
                                utils.resError(res);
                                hasError = true;
                            })
                            .on('end', () => {
                                if (!hasError) {
                                    let buffer = Buffer.concat(buffers);
                                    // attempt to load file and start print
                                    utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'file_load', [ buffer ], [], result => {
                                        // success ; set some extra properties needed for later
                                        utils.resValue(res);
                                        utils.setPrinterExtraProperty(rpc, 'loaded_file_id', '' + file._id);
                                    });
                                }
                            });
                    });
                });
            });
        });
    });
});

// start printer calibration (must be admin)
router.post('/:printerid/calibration', (req, res) => {
    utils.validateAuthAdminOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                // attempt to enter calibration mode
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'enter_calibration', []);
            });
        });
    });
});

// move while calibrating printer (must be admin)
router.patch('/:printerid/calibration', (req, res) => {
    utils.reqRequireParamsOrError(req, res, ['value', 'mode'], () => {
        utils.validateAuthAdminOrError(req, res, user => {
            utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
                utils.connectToPrinterOrCached(res, printer, rpc => {
                    // attempt to move, and answer instantly because z_move will block movement
                    utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'z_move', [ req.body.value, req.body.mode ], [], ignore => {});
                    utils.resValue(res);
                });
            });
        });
    });
});

// stop printer calibration (must be admin)
router.delete('/:printerid/calibration', (req, res) => {
    utils.validateAuthAdminOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                // attempt to exit calibration mode
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'finish_calibration', []);
            });
        });
    });
});

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//          JOB CONTROL
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// get printer status by id
router.get('/:printerid/state', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'state', [], [], state => {
                    // work callback
                    let callback = loadedFileId => {
                        // prepare result
                        let result = {};
                        if (loadedFileId !== null) {
                            result['loaded_file'] = {
                                id: loadedFileId,
                                details: state.file_details
                            };
                        }
                        if (state.power) {
                            let powered = {
                                calibrating: state.calibrating
                            };
                            if (state.z_position) {
                                powered['calibrated'] = {
                                    z_position: state.z_position
                                };
                            }
                            if (state.printing) {
                                let start = state.current_job.start_time * 1000;
                                powered['printing'] = {
                                    paused: state.current_job.pause,
                                    start_date: start,
                                    end_date: Date.now() + (state.current_job.estimated_end_time * 1000),
                                    current_layer: state.current_job.current_layer,
                                    max_layer: state.file_details.slapi.layeramount
                                };
                            }
                            result['powered'] = powered;
                        }
                        // send result
                        utils.resValue(res, result);
                    };
                    // maybe get loaded file id if loaded
                    if (state.file_details) utils.getPrinterExtraProperty(res, rpc, 'loaded_file_id', loadedFileId => callback(loadedFileId));
                    else callback(null);
                });
            });
        });
    });
});

// get printer job history by id
router.get('/:printerid/jobs/history', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.resValue(res, { job_history: !printer.job_history ? [] : printer.job_history });
        });
    });
});

// start a print
router.post('/:printerid/jobs', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'start_print', []);  // this will throw 'no loaded file' if needed
            });
        });
    });
});

// pause a print
router.patch('/:printerid/jobs/pause', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                // attempt to pause print
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'pause_print', [], [ 'Already paused' ]);
            });
        });
    });
});

// resume a print
router.patch('/:printerid/jobs/resume', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                // attempt to resume print
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'resume_print', [], [ 'Not paused' ]);
            });
        });
    });
});

// cancel a print
router.delete('/:printerid/jobs', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindPrinterOrError(res, {'_id': ObjectId(req.params.printerid)}, printer => {
            utils.connectToPrinterOrCached(res, printer, rpc => {
                // get state
                utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'state', [], [], state => {
                    if (!state.printing) {
                        utils.resError(res, 'not printing', 409);
                    } else {
                        // add to history
                        utils.getPrinterExtraProperty(res, rpc, 'loaded_file_id', loadedFileId => {
                            utils.dbPrinterAddJobHistory(rpc, printer, 'cancelled', loadedFileId, state.current_job.start_time * 1000);
                        });
                        // cancel print
                        utils.printerInvokeAndResultOrEmptyValue(res, rpc, 'cancel_print', []);
                    }
                });
            });
        });
    });
});

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// export module
module.exports = router;
