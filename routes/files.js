const express = require('express');
const formidable = require('formidable');
const fs = require('fs');
const utils = require('../utils');

let router = express.Router();

// get a list of file information (without data)
router.get('/', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFiles().find({ 'userid': user._id }).toArray().then(files => {
            let result = [];
            for (let file of files) {
                result.push({
                    id: file._id,
                    userid: file.userid,
                    name: file.name,
                    tags: file.tags,
                    upload_date: file.upload_date,
                    last_job: file.last_job
                });
            }
            utils.resValue(res, { files: result });
        });
    });
});

// get file information (without data) by id
router.get('/:fileid', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindOwnedFileOrError(res, ObjectId(req.params.fileid), user._id, file => {
            utils.resValue(res, {
                id: file._id,
                userid: file.userid,
                name: file.name,
                tags: file.tags,
                upload_date: file.upload_date,
                last_job: file.last_job
            });
        });
    });
});

// download file data by id
router.get('/:fileid/download', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindOwnedFileOrError(res, ObjectId(req.params.fileid), user._id, file => {
            gfs.createReadStream({ filename: file.gfsname }).pipe(res);  // open a stream to gfs and pipe it directly to express (no json)
        });
    });
});

// post a file for an user
router.post('/', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        // parse fields and files
        const form = formidable({ multiples: true });
        form.parse(req, (err, fields, files) => {
            // parsing error
            if (err) {
                console.log(err);
                utils.resError(res);
            } else {
                // require params
                utils.reqRequireParamsInOrError(req, res, ['tags'], fields, () => {
                    // invalid request
                    let file = files.file;
                    let tags = JSON.parse(fields.tags);
                    if (!file) {
                        utils.resError(res, 'invalid request', 422);
                    } else if (!file.name.endsWith('.sl1')) {
                        utils.resError(res, 'invalid data', 422);
                    } else if (tags.some(tag => /\s/.test(tag))) {
                        utils.resError(res, 'invalid data', 422);
                    } else {
                        // write file to db
                        let name = file.name;
                        let gfsname = user._id + '-' + name;
                        utils.dbFindFile({'gfsname': gfsname },
                            () => {
                                // insert in db
                                let stream = gfs.createWriteStream({ filename: gfsname });
                                fs.createReadStream(file.path)  // open a stream to the temporary file created by express
                                    .on('end', () => {
                                        // insert entry in db
                                        utils.dbFiles().insertOne({
                                            name: name,
                                            gfsname: gfsname,
                                            userid: user._id,
                                            tags: tags,
                                            upload_date: Date.now()
                                        }).then(insertResult => {
                                            utils.resValue(res, { id: insertResult.insertedId });
                                        });
                                    })
                                    .on('error', () => utils.resError(res))
                                    .pipe(stream);
                            },
                            // file already exists
                            file => utils.resError(res, 'name already exists', 409)
                        );
                    }
                });
            }
        });
    });
})

// delete a file by id
router.delete('/:fileid', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbFindOwnedFileOrError(res, ObjectId(req.params.fileid), user._id, file => {
            // prepare deletion callback
            let proceed = () => {
                utils.dbFiles().deleteOne({ '_id': file._id }).then(deleteResult => {
                    if (deleteResult.deletedCount === 0) {
                        utils.resError(res);
                    } else {
                        utils.resValue(res);
                        // delete file in gfs
                        db.collection('fs.files').findOne({ filename: file.gfsname }).then(elem => {
                            if (elem) {
                                db.collection('fs.chunks').remove({ files_id: elem._id });
                                db.collection('fs.files').remove({ _id: elem._id });
                            }
                        });
                    }
                });
            }
            // make sure it's not currently printing
            utils.dbPrinters().find({}).toArray().then(printers => {
                let mustDeny = [];  // list of promises, returns a boolean 'true' if operation was denied
                for (let printer of printers) {
                    mustDeny.push(new Promise((resolve, reject) => {
                        utils.connectToPrinterOrCached(null, printer, rpc => {
                            utils.printerInvokeAndResultOrEmptyValue(null, rpc, 'state', [], [], state => {
                                // file is loaded
                                if (state.file_loaded) {
                                    utils.getPrinterExtraProperty(null, rpc, 'loaded_file_id', loadedFileId => {
                                        if (loadedFileId === ('' + file._id)) {
                                            if (state.printing) {
                                                resolve(true);
                                            } else {
                                                resolve(false);
                                                // unload file
                                                utils.printerInvokeAndResultOrEmptyValue(null, rpc, 'file_unload', []);
                                            }
                                        }
                                    });
                                }
                                // not loaded
                                else {
                                    resolve(false);
                                }
                            }, err => reject(err));
                        }, err => reject(err));
                    }));
                }
                Promise.all(mustDeny)
                    .then(array => {
                        if (array.some(shouldDeny => !!shouldDeny)) utils.resError(res, 'currently printing', 409);  // refuse deletion
                        else proceed();  // accept and proceed to deletion
                    });
            });
        });
    });
});

// export module
module.exports = router;
