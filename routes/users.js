const express = require('express');
const utils = require('../utils');

let router = express.Router();

// get a list of users
router.get('/', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        utils.dbUsers().find({}).toArray().then(users => {
            let result = [];
            for (let user of users) {
                result.push({
                    id: user._id,
                    name: user.name,
                    is_admin: user.is_admin
                });
            }
            utils.resValue(res, { users: result });
        });
    });
});

// get by id
let byId = (req, res, authentifiedToken, user) => {
    utils.resValue(res, {
        id: user._id,
        name: user.name,
        is_admin: user.is_admin,
        tokens: !user.tokens ? [] : user.tokens.map(token => {
            return {
                id: token.id,
                description: token.description,
                expires: token.expires,
                is_me: token.token === authentifiedToken
            };
        })
    });
};

router.get('/@me', (req, res) => {
    utils.validateAuthOrError(req, res, user => byId(req, res, req.headers.authorization, user));
});

router.get('/:userid', (req, res) => {
    utils.dbFindUserOrError(res, {'_id': ObjectId(req.params.userid)}, user => byId(req, res, req.headers.authorization, user));
});

// create user
router.post('/', (req, res) => {
    utils.reqRequireParamsOrError(req, res, ['name', 'password', 'is_admin'], () => {
        if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{7,}$/.test(req.body.password) || !/^(true|false)$/.test(req.body.is_admin)) {
            utils.resError(res, 'invalid data', 422);
        } else {
            utils.validateAuthAdminOrError(req, res, authentifiedUser => {
                utils.dbFindUser({'name': req.body.name},
                    () => {
                        // hash password and insert in db
                        utils.hashOrError(res, req.body.password, hash => {
                            utils.dbUsers().insertOne({
                                name: req.body.name,
                                password: hash,
                                is_admin: req.body.is_admin === 'true'
                            }).then(insertResult => {
                                utils.resValue(res, { id: insertResult.insertedId });
                            });
                        });
                    },
                    // user already exists
                    user => utils.resError(res, 'name already exists', 409)
                );
            });
        }
    });
});

// modify user (must be self or admin)
router.patch('/:userid', (req, res) => {
    utils.dbFindUserOrError(res, {'_id': ObjectId(req.params.userid)}, user => {
        utils.validateAuthSelfOrAdminOrError(req, res, user, authentifiedUser => {
            // prepare modifier and validate arguments
            let modifyCallback = passwordHash => {
                let setQuery = {};
                if (req.body.name) {
                    setQuery['name'] = req.body.name;
                }
                if (req.body.is_admin !== undefined) {
                    setQuery['is_admin'] = req.body.is_admin === 'true' || req.body.is_admin === true;
                }
                if (passwordHash) {
                    setQuery['password'] = passwordHash;
                }
                if (Object.keys(setQuery).length === 0) {
                    utils.resError(res, 'invalid request', 422);
                } else {
                    // make sure it's not the last admin deranking himself
                    console.log(setQuery.is_admin);
                    console.log(user.is_admin);
                    if (setQuery.is_admin !== undefined && !setQuery.is_admin && user.is_admin) {
                        console.log('yeet');
                        utils.dbUsers().countDocuments({ is_admin: true }).then(count => {
                            if (count === 1) utils.resError(res, 'last admin user', 403);
                            else utils.dbUsers().updateOne({'_id': user._id}, { $set: setQuery }).then(updateResult => utils.resValue(res));
                        });
                    }
                    // otherwise just change
                    else {
                        utils.dbUsers().updateOne({'_id': user._id}, { $set: setQuery }).then(updateResult => utils.resValue(res));
                    }
                }
            };
            // maybe hash password and callback, or just callback instantly
            if (req.body.password) utils.hashOrError(res, req.body.password, hash => modifyCallback(hash));
            else modifyCallback(null);
        });
    });
});

// delete user (must be self or admin)
router.delete('/:userid', (req, res) => {
    utils.dbFindUserOrError(res, {'_id': ObjectId(req.params.userid)}, user => {
        utils.validateAuthSelfOrAdminOrError(req, res, user, authentifiedUser => {
            // prepare delete callback after checking if isn't last admin user
            let work = () => {
                utils.dbUsers().deleteOne({ '_id': user._id }).then(deleteResult => {
                    if (deleteResult.deletedCount === 0) {
                        utils.resError(res);
                    } else {
                        utils.resValue(res);
                    }
                });
            };
            // make sure we're not deleting the last admin user
            if (user.is_admin) {
                utils.dbUsers().countDocuments({ is_admin: true }).then(count => {
                    if (count === 1) utils.resError(res, 'last admin user', 403);
                    else work();
                });
            } else {
                work();
            }
        });
    });
});

// export module
module.exports = router;
