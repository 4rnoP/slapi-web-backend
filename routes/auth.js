const express = require('express');
const crypto = require('crypto');
const utils = require('../utils');

let router = express.Router();

// create auth token
router.post('/', (req, res) => {
    utils.reqRequireParamsOrError(req, res, ['name', 'password', 'description', 'expires'], () => {
        utils.dbFindUserOrError(res, {'name': req.body.name}, user => {
            utils.compareHashOrError(res, req.body.password, user.password,() => {
                let id = crypto.randomBytes(10).toString('hex');
                let newToken = crypto.randomBytes(20).toString('hex');
                utils.dbUsers().updateOne({'_id': user._id}, {
                    $push: { 'tokens': {
                        'id': id,
                        'token': newToken,
                        'description': req.body.description,
                        'expires': parseInt(req.body.expires)
                    }}
                }).then(updateResult => {
                    if (updateResult.modifiedCount === 0) {
                        utils.resError(res);
                    } else {
                        utils.resValue(res, { id: user._id, token: newToken });
                    }
                });
            });
        });
    });
});

// delete auth token
router.delete('/', (req, res) => {
    utils.validateAuthOrError(req, res, user => {
        // remove token
        utils.dbUsers().updateOne({'_id': user._id}, {
            $pull: { 'tokens': {
                    'token': req.headers.authorization
                } }
        }).then(updateResult => {
            if (updateResult.modifiedCount === 0) {
                utils.resError(res);
            } else {
                utils.resValue(res);
            }
        });
    });
});

// delete auth token by id
router.delete('/byid', (req, res) => {
    utils.reqRequireParamsOrError(req, res, ['id'], () => {
        utils.validateAuthOrError(req, res, user => {
            // remove token
            utils.dbUsers().updateOne({'_id': user._id}, {
                $pull: { 'tokens': {
                    'id': req.body.id
                } }
            }).then(updateResult => {
                if (updateResult.modifiedCount === 0) {
                    utils.resError(res, 'unknown token', 404);
                } else {
                    utils.resValue(res);
                }
            });
        });
    });
});

// export module
module.exports = router;
