const express = require('express');
const router = express.Router();
const path = require('path');

require("fs").readdirSync(__dirname).forEach(function(file) {
    if (file !== 'index.js') {
        router.use('/' + file.replace('.js', ''), require(path.join(__dirname, file)));
    }
});

module.exports = router;
