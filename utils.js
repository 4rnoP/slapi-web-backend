const bcrypt = require('bcrypt');
const zerorpc = require('zerorpc');

module.exports = {
    // req/res
    resError: (res, err = 'unknown error', code = 500, extra = null) => {
        if (res) {  // sometimes we use a fake request to call some utils method ; just ignore that
            let value = { error: err };
            if (extra != null) {
                value['extra'] = extra;
            }
            res.status(code).json(value);
        }
    },
    resValue: (res, val = {}) => {
        if (res) {  // sometimes we use a fake request to call some utils method ; just ignore that
            res.json({ value: val });
        }
    },
    reqRequireParamsOrError (req, res, params, ifValid) {
        this.reqRequireParamsInOrError(req, res, params, req.body, ifValid);
    },
    reqRequireParamsInOrError (req, res, params, mustCheckIn, ifValid) {
        for (let param of params) {
            let value = mustCheckIn[param];
            if (value === undefined || value === null) {
                this.resError(res, 'invalid request', 422, { param: param, found: value });
                return;
            }
        }
        ifValid();
    },
    // db
    dbUsers: () => db.collection('users'),
    dbFiles: () => db.collection('files'),
    dbPrinters: () => db.collection('printers'),
    // db : user
    dbFindUser (filter, ifNotFound, ifFound) {
        this.dbUsers().findOne(filter).then(user => {
            if (!user) ifNotFound();
            else ifFound(user);
        });
    },
    dbFindUserOrError (res, filter, ifFound) {
        this.dbFindUser(filter, () => this.resError(res, 'unknown user', 404), ifFound);
    },
    // db : file
    dbFindFile (filter, ifNotFound, ifFound) {
        this.dbFiles().findOne(filter).then(file => {
            if (!file) ifNotFound();
            else ifFound(file);
        });
    },
    dbFindFileOrError (res, filter, ifFound) {
        this.dbFindFile(filter, () => this.resError(res, 'unknown file', 404), ifFound);
    },
    dbFindOwnedFileOrError (res, fileid, userid, ifFound) {
        this.dbFindFileOrError(res, {'_id': fileid, 'userid': userid}, file => ifFound(file));
    },
    // db : printer
    dbFindPrinter (filter, ifNotFound, ifFound) {
        this.dbPrinters().findOne(filter).then(printer => {
            if (!printer) ifNotFound();
            else ifFound(printer);
        });
    },
    dbFindPrinterOrError (res, filter, ifFound) {
        this.dbFindPrinter(filter, () => this.resError(res, 'unknown printer', 404), ifFound);
    },
    dbPrinterAddJobHistory (rpc, printer, result, loadedFileId, start_date) {
        this.dbPrinters().updateOne({'_id': printer._id}, {
            $push: { 'job_history': {
                    'result': result,
                    'file_id': loadedFileId,
                    'start_date': Math.floor(start_date),
                    'end_date': Date.now()
                } }
        });
        console.log(loadedFileId);
        this.dbFiles().updateOne({'_id': ObjectId(loadedFileId)}, {
            $set: { 'last_job': {
                    'result': result,
                    'printer_id': printer._id,
                    'start_date': Math.floor(start_date),
                    'end_date': Date.now()
                } }
        });
    },
    // authentication
    validateAuthOrError (req, res, ifFound) {
        if (!req.headers.authorization) {
            this.resError(res, 'invalid request', 422, { 'param': 'authorization' });
        } else {
            this.dbFindUser(
                { 'tokens': {
                        $elemMatch: {
                            'token': req.headers.authorization,
                            $or: [
                                { 'expires': -1 },
                                { 'expires': { $gt: Date.now() } }
                            ]
                        }
                    } },
                () => this.resError(res, 'invalid token', 401),
                ifFound
            );
        }
    },
    validateAuthAdminOrError (req, res, ifFound) {
        this.validateAuthOrError(req, res, user => {
            if (!user.is_admin) this.resError(res, 'insufficient permissions', 403);
            else ifFound(user);
        });
    },
    validateAuthSelfOrAdminOrError (req, res, mustBeUserIfNotAdmin, ifFound) {
        this.validateAuthOrError(req, res, authentifiedUser => {
            if (authentifiedUser.is_admin || ('' + authentifiedUser._id) === ('' + mustBeUserIfNotAdmin._id) /* otherwise not equals for some reason */) ifFound(authentifiedUser);
            else this.resError(res, 'insufficient permissions', 403);
        });
    },
    // zerorpc
    printerClients: {},
    connectToPrinterOrCached(res, printer, ifConnected, reject = null) {
        try {
            let client = this.printerClients['' + printer._id];
            if (!client) {
                client = new zerorpc.Client({ heartbeatInterval: 2500 });
                client.connect(printer.socket_address);
                this.printerClients['' + printer._id] = client;
            }
            ifConnected(client);
        } catch (err) {
            if (reject == null) {
                this.resError(res);
            } else {
                reject(err);
            }
        }
    },
    disconnectPrinter(printer) {
        let client = this.printerClients['' + printer._id];
        if (client) {
            client.close();
            delete this.printerClients['' + printer._id];
        }
    },
    printerInvokeErrors: {
        'Currently in a print': ['currently printing', 409],
        'Currently in a calibration': ['currently calibrating', 409],
        'Not powered': ['not powered', 409],
        'Not calibrated': ['not calibrated', 409],
        'Not calibrating': ['not calibrating', 409],
        'Not printing': ['not printing', 409],
        'No file loaded': ['no loaded file', 409]
    },
    printerInvokeAndResultOrEmptyValue(res, rpc, methodName, methodParams, ignoreErrors = [], ifResult = null, reject = null) {
        let args = [];
        args.push(methodName);
        args = args.concat(methodParams);
        args.push((error, result, more) => {
            // error
            if (error) {
                if (reject != null) {
                    reject(error);
                } else if (error.message.includes('Lost remote')) {
                    this.resError(res, 'no printer connection', 504);
                } else if (ignoreErrors.includes(error.message)) {
                    this.resValue(res);
                } else {
                    let err = this.printerInvokeErrors[error.message];
                    if (err) this.resError(res, err[0], err[1]);
                    else this.resError(res);
                }
            }
            // result
            else if (ifResult) {
                ifResult(result);
            } else {
                this.resValue(res);
            }
        });
        rpc.invoke.apply(rpc, args);
    },
    setPrinterExtraProperty(rpc, property, data) {
        rpc.invoke('extra_property_set', property, data, (error, result, more) => {});
    },
    getPrinterExtraProperty(res, rpc, property, ifDone) {
        rpc.invoke('extra_property_get', property, (error, result, more) => {
            if (error) {
                this.resError(res);
            } else {
                ifDone(result);
            }
        });
    },
    // misc
    hashOrError (res, str, ifSuccess) {
        bcrypt.hash(str, 10, (err, hashed) => {
            if (err) this.resError(res);
            else ifSuccess(hashed);
        });
    },
    compareHashOrError (res, plain, hashed, ifSuccess) {
        bcrypt.compare(plain, hashed, (err, result) => {
            if (err) this.resError(res);
            else if (result === true) ifSuccess();
            else this.resError(res, 'invalid credentials', 401);
        });
    }
};
